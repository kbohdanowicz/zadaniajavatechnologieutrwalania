## Treści rozdziału

1. JDBC - informacje wstępne
1. Baza danych [HSQL](http://hsqldb.org) i skrypty startowe
1. Struktura projektu, [Apache Maven](https://maven.apache.org)
1. Setup projektu - sterownik, URL
1. Pierwszy program
1. Praca z zapytaniami w JDBC
1. [Interface Statement](https://docs.oracle.com/javase/8/docs/api/java/sql/Statement.html#executeQuery-java.lang.String-), metoda executeUpdate()
1. [Interface Statement](https://docs.oracle.com/javase/8/docs/api/java/sql/Statement.html#executeQuery-java.lang.String-), metoda executeQuery()
1. Praca z [ResultSet](https://docs.oracle.com/javase/8/docs/api/java/sql/ResultSet.html)
