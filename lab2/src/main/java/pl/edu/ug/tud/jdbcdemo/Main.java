package pl.edu.ug.tud.jdbcdemo;

import java.net.ConnectException;
import java.sql.*;

public class Main {

  public static void main(String[] args) throws SQLException {

    final String DB_URL = "jdbc:hsqldb:hsql://localhost/workdb";

    final String INSERT_STMT = "INSERT INTO Person VALUES(?, ?, ?)";
    final String SELECT_STMT = "SELECT * from Person";
    final String UPDATE_STMT = "UPDATE Person SET yob = ? WHERE name = ?";
    final String DELETE_STMT = "DELETE FROM Person WHERE name = ?";

      Connection con = DriverManager.getConnection(DB_URL);

      PreparedStatement insertPersonPST = con.prepareStatement(INSERT_STMT);
      PreparedStatement selectPersonPST = con.prepareStatement(SELECT_STMT);
      PreparedStatement updatePersonPST = con.prepareStatement(UPDATE_STMT);
      PreparedStatement deletePersonPST = con.prepareStatement(DELETE_STMT);

      insertPersonPST.setLong(1,4);
      insertPersonPST.setString(2,"Barney");
      insertPersonPST.setInt(3,1998);

      insertPersonPST.executeUpdate();

      updatePersonPST.setInt(1,1950);
      updatePersonPST.setString(2,"John");

      updatePersonPST.executeUpdate();

      deletePersonPST.setString(1,"Barney");

      deletePersonPST.executeUpdate();

      ResultSet rsQuery = selectPersonPST.executeQuery();

      while (rsQuery.next()) {
        long id = rsQuery.getLong("id");
        String name = rsQuery.getString("name");
        int yob = rsQuery.getInt("yob");
        System.out.println(id + "\t" + name + "\t" + yob);
      }
  }
}

/*
--CREATE TABLE Person (id BIGINT, name VARCHAR(50), yob INT)
--INSERT INTO Person VALUES (1, 'John', 1990), (2, 'Andy', 1991), (3, 'Charles', 1992)
 */
